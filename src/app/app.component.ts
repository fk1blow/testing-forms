import { Component } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';

enum SectionType {
  Foo = 'Foo',
  Bar = 'Bar'
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  sectionsForm = new FormGroup({
    sections: this.fb.array([
      this.fb.group({
        name: new FormControl('', [Validators.required]),
        type: new FormControl(SectionType.Foo)
      })
    ])
  })

  constructor(private fb: FormBuilder) {
    console.log('this.sectionsForm :', this.sectionsForm);
  }

  get sectionsFormArray() {
    return this.sectionsForm.get('sections') as FormArray
  }

  getControlFromSection(section: FormGroup, control: string) {
    return section.get(control) as FormControl
  }

  onAddSection() {
    const newGroup = this.fb.group({
      name: new FormControl('', [Validators.required]),
      type: new FormControl(SectionType.Bar)
    })

    this.sectionsFormArray.push(newGroup)
  }

}
